const merge = require('deepmerge')

module.exports = function (overrides = {}) {
  const base = {
    root: true,
    extends: [
      'standard'
    ],
    ignorePatterns: [
      'dist/'
    ],
    overrides: [
      {
        files: [
          '*.js'
        ],
        rules: {
          'no-new-func': 'off'
        }
      }
    ]
  }

  const options = {
    arrayMerge: (destArray, sourceArray) => [...destArray, ...sourceArray]
  }

  return merge(base, overrides, options)
}
