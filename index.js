#!/usr/bin/env node

// eslint-disable-next-line
require('yargs')
  .commandDir('./cmd')
  .demandCommand()
  .help()
  .argv
