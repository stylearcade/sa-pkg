module.exports = {
  root: true,
  extends: [
    'standard'
  ],
  ignorePatterns: [
    'templates/*'
  ]
}
