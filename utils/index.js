const fs = require('fs')
const merge = require('deepmerge')
const md5 = require('md5')
const path = require('path')
const { spawn } = require('child_process')
const sortPackageJson = require('sort-package-json')
const sortObjectKeys = require('sort-object-keys')

async function retry (func, options = {}) {
  const { maxAttempts = Infinity } = options

  let attempts = 0

  while (true) {
    try {
      const result = await func()
      return result
    } catch (e) {
      if (++attempts === maxAttempts) throw e
    }
  }
}

function exec (cwd, command, options = {}) {
  let { args, maxAttempts = 1 } = options

  if (command.includes(' ') && args === undefined) {
    [command, ...args] = command.split(' ')
  }

  return retry(() => new Promise((resolve, reject) => {
    spawn(command, args, {
      env: process.env,
      cwd,
      stdio: 'inherit'
    }).on('close', (code) => {
      if (code === 0) resolve()
      else reject(Error(`${cwd} ${command} exited with code ${code}`))
    })
  }), { maxAttempts })
}

function updatePackage (pkgDir, mergeData) {
  const pkgFile = path.join(pkgDir, 'package.json')

  let pkg = require(pkgFile)

  pkg = merge(pkg, mergeData)
  pkg = sortPackageJson(pkg)
  pkg.scripts = sortObjectKeys(pkg.scripts)

  fs.writeFileSync(pkgFile, JSON.stringify(pkg, null, 2) + '\n')

  console.log(`Updated ${pkgFile}`)
}

function copyFile (templateDir, pkgDir, file) {
  const src = path.join(templateDir, file)
  const dst = path.join(pkgDir, file)

  fs.copyFileSync(src, dst)
  console.log(`Replaced ${dst}`)
}

async function install (pkgDir, installCmd = 'npm i', execOpts = {}) {
  const filePath = path.join(pkgDir, '.sa-cache-key.md5')

  // hash package.json
  const cr = /\r/g
  const packageStr = fs.readFileSync(path.join(pkgDir, 'package.json')).toString().replace(cr, '')
  const nextHash = md5(packageStr)

  // load previous hash
  const prevHash = fs.existsSync(filePath)
    ? fs.readFileSync(filePath).toString()
    : null

  if (nextHash !== prevHash) {
    // install
    try {
      await exec(pkgDir, installCmd, execOpts)
    } catch (e) {
      console.log('failed', pkgDir)
    }

    // save new hash
    fs.writeFileSync(filePath, nextHash)
  }
}

module.exports = {
  retry,
  exec,
  updatePackage,
  copyFile,
  install
}
