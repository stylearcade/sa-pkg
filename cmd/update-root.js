const { updatePackage } = require('../utils')

module.exports = {
  command: 'update-root',
  description: 'update a root package',
  handler: async (argv) => {
    const pkgDir = process.cwd()

    await updatePackage(pkgDir, require('../templates/root/package.json'))
  }
}
