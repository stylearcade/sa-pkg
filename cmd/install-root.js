const { exec, updatePackage } = require('../utils')

module.exports = {
  command: 'install-root',
  description: 'install sa-pkg into a root package',
  handler: async () => {
    const pkgDir = process.cwd()

    await updatePackage(pkgDir, require('../templates/root/package.json'))

    await exec(pkgDir, 'npm run update-pkg')
  }
}
