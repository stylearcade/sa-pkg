const path = require('path')
const { updatePackage, copyFile } = require('../utils')

module.exports = {
  command: 'update-ts',
  description: 'update a ts-package',
  handler: async (argv) => {
    const pkgDir = process.cwd()
    const templateDir = path.join(__dirname, '../templates/ts')

    await updatePackage(pkgDir, require('../templates/ts/package.json'))

    copyFile(templateDir, pkgDir, '.eslintrc.js')
    copyFile(templateDir, pkgDir, 'tsconfig.json')
  }
}
