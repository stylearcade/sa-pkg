const path = require('path')
const merge = require('deepmerge')
const callsite = require('callsite')

module.exports = function (overrides = {}) {
  const stack = callsite()
  const callerFilename = stack[1].getFileName()
  const callerDirectory = path.dirname(callerFilename)

  const base = {
    root: true,
    extends: [
      'standard-with-typescript'
    ],
    parserOptions: {
      project: './tsconfig.json',
      tsconfigRootDir: callerDirectory
    },
    ignorePatterns: [
      'dist/'
    ],
    overrides: [
      {
        files: [
          '*.ts'
        ],
        extends: [
          'plugin:@typescript-eslint/recommended'
        ],
        plugins: [
          '@typescript-eslint/eslint-plugin'
        ],
        rules: {
          'no-new-func': 'off',
          '@typescript-eslint/no-var-requires': 'off',
          '@typescript-eslint/no-implied-eval': 'off'
        }
      },
      {
        files: [
          '*.spec.ts'
        ],
        rules: {
          // These rules enable loose async code in tests
          '@typescript-eslint/no-floating-promises': 'off',
          '@typescript-eslint/no-invalid-void-type': 'off',
          '@typescript-eslint/no-misused-promises': 'off',
          '@typescript-eslint/promise-function-async': 'off',
          '@typescript-eslint/return-await': 'off'
        }
      }
    ]
  }

  const options = {
    arrayMerge: (destArray, sourceArray) => [...destArray, ...sourceArray]
  }

  return merge(base, overrides, options)
}
